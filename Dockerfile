FROM golang:1.18.2-alpine3.15

WORKDIR /app

COPY . /app

RUN apk update && apk upgrade && \
    go get -u github.com/joho/godotenv && \
    go get -u github.com/gorilla/mux && \
    go get -u github.com/go-sql-driver/mysql && \
    go get -u aws-s3-sample/aws-s3-service/repository && \
	go get -u aws-s3-sample/aws-s3-service/service && \
	go get -u aws-s3-sample/user/service && \
	go get -u aws-s3-sample/user/repository && \
	go get -u aws-s3-sample/handler && \
	go get -u aws-s3-sample/handler/page-handler


RUN go build -o /tmp/app

EXPOSE 8081

CMD ["/tmp/app"]